function createCard(name, description, pictureUrl, startDate, endDate, location) {
    startDate = new Date(startDate);
    endDate = new Date(endDate);
    const formattedStartDate = `${startDate.getMonth() + 1}/${startDate.getDate()}/${startDate.getFullYear()}`;
    const formattedEndDate = `${endDate.getMonth() + 1}/${endDate.getDate()}/${endDate.getFullYear()}`;
  
    return `
      <div class="card mb-4 mt-4 shadow">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p class="card-location">${location}</p>
          <p class="card-text">${description}</p>
        </div>
        <footer class="card-footer">
          <p class="card-dates">${formattedStartDate} - ${formattedEndDate}</p>
        </footer>
      </div>
    `;
  }

  
  
  window.addEventListener("DOMContentLoaded", async () => {
    const url = "http://localhost:8000/api/conferences/";
    const alertContainer = document.querySelector("#alert-container");
    function showAlert(message) {
      alertContainer.innerHTML = `
      <div class="alert alert-danger alert-dismissible fade show" role="alert">
        ${message}
       <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>
      `;
    }
  
    try {
      const response = await fetch(url);
  
      if (!response.ok) {
        showAlert("There was a problem fetching the conferences list.");
      } else {
        const data = await response.json();
  
        let dds = 0;
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const location = details.conference.location.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = details.conference.starts;
            const endDate = details.conference.ends;
            const html = createCard(name, description, pictureUrl, startDate, endDate, location);
            const column = document.querySelector(`#col_${dds % 3}`);
            column.innerHTML += html;
            dds++;
          }
        }
      }
    } catch (e) {
      console.error(e);
      showAlert("An unexpected error occurred.");
    }
  });