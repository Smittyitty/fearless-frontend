from django.urls import path
from events.api_views import api_list_states

from .api_views import api_list_accounts, api_account_detail

urlpatterns = [
    path("states/", api_list_states, name="api_list_states"),
    path("accounts/", api_list_accounts, name="api_list_accounts"),
    path(
        "accounts/<str:email>/",
        api_account_detail,
        name="api_account_detail",
    ),
]
